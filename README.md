# LF2 - Ausschreibung 2 (Fortgeschritten)

## Anforderungen
- Anwendung kann den Fahrpreis von gemieteten E-Scootern nach Nutzungsdauer berechnen.
- Anwendung wird von Benutzer über eine GUI gesteuert.
- Ausgaben werden über die GUI zurückgegeben.
- Eine grafische Oberfläche ist weitesten implementiert.
- Eine Benutzerverwaltung ist vorhanden
- Weitere Erweiterungen (Password hashing, Anmeldefunktion, usw.)wurden implementiert

### Arbeitsvorgehensweise

1. Implementierung: Einloggen: 
   - Kunde name + password
   - falls kunde vorhanden ist begrüssen (kleine interaktion)
   - falls kunde nicht vorhanden ist - Neuanmeldung ermöglichen
2. Start button einbauen
3. Stop button einbauen
4. Implementierung der Nutzungsdauer- und Kostenkalkulation
5. Start, Stopp, der Preis und Nutzungsdauer zeigen
6. Fahrtinformation in der Datenbank speichern
7. Aufbau der Profile-seite und Anzeige der Fahrthistorie  
8. Implementierung: Ausloggen 


### Pre-installation um Program auszuführen

```console
source venv/bin/activate
pip install flask_sqlalchemy
pip install flask_migrate
pip install flask_login
pip install flask_wtf
pip install email_validator
python -m flask run   #(python3 für einige windows Versionen/ flask run für Linux Systeme)
```

### Flask shell Befehle um mit der Datenbank zu kommunizieren
```console
import app
from app.models import User, Ride
from app import db
u = User.query.filter_by(username= "test").first()
from datetime import datetime
r = Ride(start= datetime.utcnow, stop=datetime.utcnow ,minutes = 10, costs= 5, author=u)
db.session.add(r,u)
db.session.commit()
```

### Mögliche weitere Schritte
- Datenbank auf ein Server hosten.
- Verschiedene Scooter-Typen anbieten.
- End-to-End Tests implementieren.
- AGB, rechtliche Hinweise und Datenschutz selbst implementieren.
- CSS Stylen verbessern.