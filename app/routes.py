from app import app, db
from flask import render_template, flash, redirect, url_for, request
from app.forms import LoginForm, RideForm
from flask_login import current_user, login_user, login_required
from app.models import User, Ride
from flask_login import logout_user
from app.forms import RegistrationForm
from calculations import click_start, format_date
from datetime import datetime


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    form = RideForm()
    # if form.validate_on_submit():
    if request.method == "POST":
        stat_input = request.form.get("start_input")
        # start = datetime.strptime(form.start.data,  "%d.%m.%Y, %H:%M:%S")
        start = datetime.strptime(stat_input, "%d.%m.%Y, %H:%M:%S")
        # start = form.start
        stop_input = request.form.get("stop_input")
        # stop = datetime.strptime(form.start.data,  "%d.%m.%Y, %H:%M:%S")
        stop = datetime.strptime(stop_input, "%d.%m.%Y, %H:%M:%S")
        # minutes = form.minutes.data
        minutes = request.form.get("minutes_input")
        # costs = form.costs.data
        costs = request.form.get("costs_input")
        print(start, stop, minutes, costs)
        ride = Ride(start=start, stop=stop, minutes=minutes, costs=costs, author=current_user)
        db.session.add(ride)
        db.session.commit()
        flash('Your ride is successfully saved')
        return redirect(url_for('index'))
    user = current_user
    rides = current_user.rides
    return render_template('index.html', title='Home', user=user, form=form, rides=rides)


@app.route('/login', methods=['GET', 'POST'])
def login():
    print("hallo")
    if current_user.is_authenticated:
        print("already logged in")
        return redirect(url_for('index'))
    form = LoginForm()
    print("form.validate_on_submit():", form.validate_on_submit())
    if form.validate_on_submit():
        print(" in first if")
        user = User.query.filter_by(username=form.username.data).first()
        print(user)
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/user', methods=['GET', 'POST'])
@login_required
def user():
    user = current_user
    rides = user.rides.order_by(Ride.start.desc())[0:8]
    return render_template('user.html', user=user, rides=rides)


@app.route('/start')
def start_timer():
    flash("Dear customer your begin time: ", format_date(click_start()))
    return "Dear customer your begin time: ", format_date(click_start())


@app.route('/impressum', methods=['GET'])
@login_required
def impressum():
    user = current_user
    return render_template('impressum.html', user=user)
