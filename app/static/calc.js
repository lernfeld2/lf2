const costs_per_min = 0.50;
let start_date = null
let stop_date = null
function createDate() {
    return new Date()
}

function click_start(){
    let start_time_div = document.getElementById("start_time_div");
    start_date = createDate()
    start_time_div.innerHTML = start_date.toLocaleString();
    document.getElementById("start_button").disabled = true;
    document.getElementById("stop_button").disabled = false;
    document.getElementById("total_rent_time").innerHTML = "";
    document.getElementById("stop_time_div").innerHTML= "";
}

function click_stop(){
    let stop_time_div = document.getElementById("stop_time_div");
    stop_date = getRandomDate()
    stop_time_div.innerHTML = stop_date.toLocaleString();
    document.getElementById("stop_button").disabled = true;


    const total_min = get_total_min(get_time_diff(start_date, stop_date))

    // const total_cost = get_total_costs(total_min,costs_per_min)
    const total_cost = (Math.round(get_total_costs(total_min,costs_per_min))).toFixed(2);

    let total_rent_time = document.getElementById("total_rent_time");
    const date_diff_s = date_diff_string(get_time_diff(start_date,stop_date))
    total_rent_time.innerHTML = "Thank you for using our scooter, your total usage time is: " + date_diff_s+  " and you will be charged: " + total_cost + " €."
    document.getElementById("start_button").disabled = false;
    document.getElementById("stop_button").disabled = true;
    console.log("start date: ",start.toLocaleString())
    console.log("end date: ",end.toLocaleString())
    console.log(get_time_diff(start, end))
    console.log("string: ", date_diff_string(get_time_diff(start, end)))
    console.log("total min: ", total_min)
    console.log("total cost: ", total_cost);

}

function get_time_diff(start, end) {
    let datetime = end.getTime();
    let now = start.getTime();

    if (isNaN(datetime)) {
        return "";
    }

    console.log(datetime + " " + now);
    let milisec_diff = null;
    if (datetime < now) {
        milisec_diff = now - datetime;
    } else {
        milisec_diff = datetime - now;
    }
    let date_diff = new Date(milisec_diff);
    return date_diff
}

function date_diff_string(date_diff) {
    const h = (date_diff.getHours()) - 1
    return h + " Hours " + date_diff.getMinutes() + " Minutes " + date_diff.getSeconds() + " Seconds";
}



function get_total_min(date_diff) {
    const h = (date_diff.getHours()) - 1
    const h_to_min = h * 60
    return h_to_min + date_diff.getMinutes();
}

function getRandomDate() {
    const start = createDate();
    const end = new Date(2022, 6, 2)
    const timestamp = start.getTime() + Math.random() * (end.getTime() - start.getTime());
    return new Date(timestamp);
}

function get_total_costs(total_min , costs_per_min){
    return total_min * costs_per_min;
}

const start = createDate()
const end = getRandomDate()

// const total_min = get_total_min(get_time_diff(start, end))
// const total_cost = get_total_costs(total_min,costs_per_min)
// console.log("start date: ",start.toLocaleString())
// console.log("end date: ",end.toLocaleString())
// console.log(get_time_diff(start, end))
// console.log("string: ", date_diff_string(get_time_diff(start, end)))
// console.log("total min: ", total_min)
// console.log("total cost: ", total_cost);