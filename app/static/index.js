const costs_per_min = 0.50;
let start_date = null
let stop_date = null

function clickStart() {
    start_date = new Date()
    document.getElementById("start_display").innerHTML = "Start: " + start_date.toLocaleString('de-DE')
    document.getElementById("start_btn").disabled = true;
    document.getElementById("stop_btn").disabled = false;
    document.getElementById("stop_btn").style = "display = block";

}

function clickStop() {
    let start_in = document.getElementById("start_input")
    start_in.value = start_date.toLocaleString('de-DE')

    stop_date = getRandomDate()
    let stop_in = document.getElementById("stop_input")
    stop_in.value = stop_date.toLocaleString('de-DE');
    document.getElementById("stop_display").innerHTML = "Stop: " + stop_date.toLocaleString('de-DE')

    const total_min = get_total_min(get_time_diff(start_date, stop_date))
    let minutes_in = document.getElementById("minutes_input")
    minutes_in.value = total_min.toString()

    // const total_cost = get_total_costs(total_min,costs_per_min)
    const total_cost = (Math.round(get_total_costs(total_min, costs_per_min))).toFixed(2);
    let costs_in = document.getElementById("costs_input")
    costs_in.value = total_cost.toString()

//    let total_rent_time = document.getElementById("ride_time_and_costs_display");
//    const date_diff_s = date_diff_string(get_time_diff(start_date, stop_date))
//    total_rent_time.innerHTML = "Thank you for using our scooter, your total usage time is: " + date_diff_s + " and you will be charged: " + total_cost + " €."

    document.getElementById("stop_btn").disabled = true;
    toggleModal()
}


function date_diff_string(date_diff) {
    const h = (date_diff.getHours()) - 1
    return h + " Hours, " + date_diff.getMinutes() + " Minutes and " + date_diff.getSeconds() + " Seconds";
}


function get_total_min(date_diff) {
    const h = (date_diff.getHours()) - 1
    const h_to_min = h * 60
    return h_to_min + date_diff.getMinutes();
}

function getRandomDate() {
    const start = new Date();
    const end = new Date(2022, 6, 6)
    const timestamp = start.getTime() + Math.random() * (end.getTime() - start.getTime());
    return new Date(timestamp);
}

function get_total_costs(total_min, costs_per_min) {
    return total_min * costs_per_min;
}

function get_time_diff(start, end) {
    let datetime = end.getTime();
    let now = start.getTime();

    if (isNaN(datetime)) {
        return "";
    }

    console.log(datetime + " " + now);
    let milisec_diff = null;
    if (datetime < now) {
        milisec_diff = now - datetime;
    } else {
        milisec_diff = datetime - now;
    }
    let date_diff = new Date(milisec_diff);
    return date_diff
}

function toggleModal() {
    const modalEl = document.querySelector('#modal')
    const modal = bootstrap.Modal.getOrCreateInstance(modalEl);
    modal.toggle();
    const modalBody = modalEl.querySelector('.modal-body');
    let date_diff_s = date_diff_string(get_time_diff(start_date, stop_date))
    const total_min = get_total_min(get_time_diff(start_date, stop_date))
    const total_cost = (Math.round(get_total_costs(total_min, costs_per_min))).toFixed(2);
    modalBody.innerHTML = `Your start time is <strong>(${start_date.toLocaleString('de-DE')})</strong> <br>
your Stop time is <strong> (${stop_date.toLocaleString('de-DE')}) </strong> <br>
 Thank you for using our scooter, your total usage time is: <strong>(${date_diff_s})</strong> and you will be charged: <strong>(${total_cost}€)</strong>.`;
    modalEl.querySelector('#confirm').addEventListener('click', function () {
        modal.hide();
        location.reload();
    })
}