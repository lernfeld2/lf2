"""ride

Revision ID: 821e0be1238d
Revises: 94066b9fb5db
Create Date: 2022-07-03 11:18:43.430100

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '821e0be1238d'
down_revision = '94066b9fb5db'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('ride', schema=None) as batch_op:
        batch_op.create_foreign_key(None, 'user', ['user_id'], ['id'])

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('ride', schema=None) as batch_op:
        batch_op.drop_constraint(None, type_='foreignkey')

    # ### end Alembic commands ###
